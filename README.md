# webcli-node-dev

A Docker image wit browser based terminals provided by node.

## Overview

This repo is a developer environment for a Docker image.

* Built on top of nodejs
* Runs a node module called `wetty` to provide terminal emulation over http (default port 3000).
* Provides commands to sync users credentials, using `passwd` format `username:password` strings.
* On startup, creates a default terminal user and/or a list of users provided in the container instance.

## Deployment

To deploy the image, use `docker-compose up` from the folder with `docker-compose.yml`. The 

## Accessing the Terminal

Visit [localhost:3000](), once the container is running.

## Volumes

The provided `docker-compose.yml` maps the parent folder to `/host/`

## User Management

Default credentials: *USER*:`term` *PASSWORD*:`term`

### EZ add users

To quickly add users, mount a `chpasswd` compatible file at `/host/users.passwd`.

If using the provided `docker-compose.yml`, create the `users.passwd` file in the same folder.

### passwd files

`/etc/passwd.list` defines the list of `chpasswd` compatible files to processes during startup.

By default it searches for:

* `/image/users.passwd`
* `/host/users.passwd`

Replace this file in the image or with volumes to change the file paths to be processed.

### Removing default user

The default user is defined in `/image/users.passwd`.

You can:

* override or remove the `chpasswd` file at `/image/users.passwd`

OR

* remove `/image/users.passwd` from `/etc/passwd.list`